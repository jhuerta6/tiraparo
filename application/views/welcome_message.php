<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>MiUni | Homepage</title>
    <style>
    .truncate {
      text-overflow: ellipsis;
    }
    .pagination {
      justify-content: center;
    }
    </style>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url()?>css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url()?>css/logo-nav.css" rel="stylesheet">
  </head>
  <body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#"> MiUni
          <!--<img src="http://placehold.it/300x60?text=Logo" width="150" height="30" alt="">-->
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="#">Bolsa de Trabajo
                <!--<span class="sr-only">(current)</span>-->
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Bazar</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Rides</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
			<div class="row">
				<div class="col text-center">
					<img class="d-none d-sm-inline" width="500" height="236" src="http://www.uacj.mx/comunicacion/PublishingImages/Escudo%20UACJ%202015/firma%20institucional%20uacj-horizontal-%202015-azul-sin%20fondo.png"></img>
					<img class="d d-sm-none" width="200" height="100" src="http://www.uacj.mx/comunicacion/PublishingImages/Escudo%20UACJ%202015/firma%20institucional%20uacj-horizontal-%202015-azul-sin%20fondo.png"></img>
				 	<br><h1>Bienvenido a: <br>MiUni - UACJ</h1> <br><br> <h4><em>Ayudando a tu economía, de una forma <strong>segura</strong> y <strong>confiable</strong>. </em></h4><br><br>
				</div>
			</div>
      <div class="row">
				<div class="col">
					<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		        <ol class="carousel-indicators">
		          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
		        </ol>
		        <div class="carousel-inner">
		          <div class="carousel-item active">
		            <!--<img class="d-block w-100" src="http://placehold.it/1330X800" alt="First slide">-->
								<img class="d-block w-100" src="http://images.huffingtonpost.com/2015-07-28-1438118656-5288874-find_high_paying_job2-thumb.jpg" alt="First slide">
		            <div class="carousel-caption d-none d-md-block"><em>
		              <h3>¡Encuentra Trabajo!</h3>
		              <p>Dirigete a nuestra Bolsa de Trabajo donde podras encontrar trabajos y practicas profesionales</p></em>
		            </div>
		          </div>
		          <div class="carousel-item">
		            <img class="d-block w-100" src="https://www.carloanssofast.com/images/girl.jpg" alt="Second slide">
		            <div class="carousel-caption d-none d-md-block"><em>
		              <h3>¡Consigue un Aventon a Clases!</h3>
		              <p>Pide un Aventon o haz uno para ir a clases</p></em>
		            </div>
		          </div>
		          <div class="carousel-item">
		            <img class="d-block w-100" src="http://www.abc.es/Media/201207/03/descenso-venta-libros--644x362.jpg" alt="Third slide">
		            <div class="carousel-caption d-none d-md-block"><em>
		              <h3>¡Compra y Vende!</h3>
		              <p>En el Bazar puedes formar parte de un ecosistema de estudiantes que comercian entre si</p></em>
		            </div>
		          </div>
		        </div>
		        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		          <span class="sr-only">Previous</span>
		        </a>
		        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		          <span class="carousel-control-next-icon" aria-hidden="true"></span>
		          <span class="sr-only">Next</span>
		        </a>
		      </div>
				</div>
			</div>

			<div class="container">
				<div class="row">
					<div class="col-sm-6"><br>
						<div class="card">
							<img class="card-img-top" src="https://t2.salir.ltmcdn.com/es/places/9/5/6/img_126659_ragtime-libros_0_orig.jpg" alt="Card image cap">
							<div class="card-block">
								<div class="container text-center">
									<p class="card-text">
										Intercambia, compra, o vende articulos varios.
									</p>
									<a href="#" class="btn btn-primary">¡Ve al Bazar!</a>
								</div><br>
							</div>
						</div>
					</div>
					<div class="col-sm-6"><br>
						<div class="card">
							<img class="card-img-top" src="http://www.gouletcommunications.com/wp-content/uploads/2014/08/Dollarphotoclub_53718856cropped.jpg" alt="Card image cap">
							<div class="card-block">
								<div class="container text-center">
									<p class="card-text">Comparte un viaje hacia tu universidad con un estudiante.</p>
									<a href="#" class="btn btn-primary">¡Consigue un Aventon!</a>
								</div><br>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6"><br>
							<div class="card">
								<img class="card-img-top" src="http://noticias.universia.net.mx/net/images/practicas-empleo/l/la/la-/la-importancia-de-la-practica-profesional.jpg" alt="Card image cap">
								<div class="card-block">
									<div class="container text-center">
										<p class="card-text">Se parte de las mas grandes compañias.</p>
										<a href="#" class="btn btn-primary">¡Explora Trabajos!</a>
									</div><br>
								</div>
							</div>
						</div>
						<div class="col-sm-6"><br>
							<div class="card">
								<img class="card-img-top" src="http://staticf5a.lavozdelinterior.com.ar/sites/default/files/styles/landscape_1020_560/public/nota_periodistica/Trabajo-freelance.jpg" alt="Card image cap">
								<div class="card-block">
									<div class="container text-center">
										<p class="card-text">Desarrolla tus habilidades con expertos en su campo.	</p>
										<a href="#" class="btn btn-primary">¡Descubre Practicas!</a>
									</div><br>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br>
		</div>
    <!-- /.container -->

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url()?>jquery/jquery.min.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap.bundle.min.js"></script>
    <script>
    $(document).ready(function() {
			$('.carousel').carousel({
		    interval: 3000
		  });
    });
    </script>
	</div>
  </body>

	<footer class="footer bg-dark text-white">
        <span>Made with <3 -- t. BlockChainers</span>
    </footer>

</html>
