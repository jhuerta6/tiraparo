<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>MiUni | Login</title>
    <style>
    .truncate {
      text-overflow: ellipsis;
    }
    .pagination {
      justify-content: center;
    }
    </style>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>css/logo-nav.css" rel="stylesheet">
  </head>
  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#"> MiUni
          <!--<img src="http://placehold.it/300x60?text=Logo" width="150" height="30" alt="">-->
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link active" href="#">Login
                <span class="sr-only">(current)</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
      <div class="row text-center">
        <div class="col">
          <form>
            <div class="form-group"><BR>
              <label for="exampleInputEmail1">Correo Electronico</label>
              <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Entra email">
              <small id="emailHelp" class="form-text text-muted">Nunca compartas tu contraseña con nadie.</small>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>

          </form>
          <button id="btnLog" class="btn btn-primary">Submit</button>
        </div>
      </div>
    </div>
    <!-- /.container -->

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url();?>jquery/jquery.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.bundle.min.js"></script>
    <script>
    $(document).ready(function() {
      $("#btnLog").click(function(){
        //console.log("lol");
        window.location.href = "<?php echo base_url();?>";
        window.location.replace("<?php echo base_url();?>")
      });

    function subir(){

    }
  });
    </script>
  </body>
</html>
