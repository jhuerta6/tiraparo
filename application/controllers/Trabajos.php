<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trabajos extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$getAllJobs = $this->getAllJobs();
		$getAllPracticas = $this->getAllPracticas();
		$getAllPracticasDisponibles = $this->getAllPracticasDisponibles();
		$getAllJobsDisponibles = $this->getAllJobsDisponibles();
		//$insert = $this->insert($eCompany, $ePosition, $eDescription, $ePractica);
		$data = array('trabajos'=>$getAllJobs, 'practicas'=>$getAllPracticas,
		'practicas_disponibles'=>$getAllPracticasDisponibles, 'trabajos_disponibles'=>$getAllJobsDisponibles);
		$this->load->view('trabajos/bolsa',$data);
		//$this->load->view('trabajos/load_all');
	}

	public function getAllJobs(){
		$this->load->model('trabajos_Model');
		return $this->trabajos_Model->getAllJobs();
	}

	public function getAllPracticas(){
		$this->load->model('trabajos_Model');
		return $this->trabajos_Model->getAllPracticas();
	}

	public function getAllPracticasDisponibles(){
		$this->load->model('trabajos_Model');
		return $this->trabajos_Model->getAllPracticasDisponibles();
	}

	public function getAllJobsDisponibles(){
		$this->load->model('trabajos_Model');
		return $this->trabajos_Model->getAllJobsDisponibles();
	}

	public function insert(){
		$this->load->model('trabajos_Model');
		$eCompany = $this->input->post('eCompany');
		$ePosition = $this->input->post('ePosition');
		$eDescription = $this->input->post('eDescription');
		$ePractica = $this->input->post('ePractica');
		return $this->trabajos_Model->insert($eCompany, $ePosition, $eDescription, $ePractica);
	}
}
